#! /usr/bin/python3

import psycopg2
import sys
from os import environ

INTERVAL = environ['OLD_INTERVAL']
USER = environ['POSTGRES_USER']
DATABASE = environ['POSTGRES_DB']
PASSWORD = environ['POSTGRES_PASSWORD']
HOST = environ['DB_HOST']
PORT = environ['DB_PORT']

print("===== Begin of deleteOldPad job =====")

#Connect to database
try:
    db = psycopg2.connect("user = " + USER + " dbname = " +DATABASE + " password = " + PASSWORD + " host = " + HOST + " port = " + PORT)
except:
    sys.exit("deleteOldPad : Unable to connect database")

with db :
    with db.cursor() as cur:
        #Get all pads older than INTERVAL
        try:
            cur.execute("SELECT \"Notes\".\"id\", \"Notes\".\"content\" FROM \"Notes\" WHERE \"Notes\".\"updatedAt\" < NOW() - interval %s", (INTERVAL,))
        except:
            sys.exit("deleteOldPad : unable to select old pads")
        oldPads = cur.fetchall()
        for oldPad in oldPads:
            #Backup the pad
            with open("deletedPads/" + oldPad[0] + '.txt', "w") as f:
                f.write(oldPad[1])
            #Remove all Revisions related to this pad
            try:
                cur.execute("DELETE FROM \"Revisions\" WHERE \"Revisions\".\"noteId\" = %s", (oldPad[0],))
            except:
                sys.exit("deleteOldPad : unable to delete all revisions related to pad " + oldPad[0])
            #Remove all contributions related to this pad
            try:
                cur.execute("DELETE FROM \"Authors\" WHERE \"Authors\".\"noteId\" = %s", (oldPad[0],))
            except:
                sys.exit("deleteOldPad : unable to delete all contributions related to pad " + oldPad[0])
            #Remove the pad
            try:
                cur.execute("DELETE FROM \"Notes\" WHERE \"Notes\".\"id\" = %s", (oldPad[0],))
            except:
                sys.exit("deleteOldPad : unable to delete the pad " + oldPad[0])

        print("deleteOldPad : Deleted old pad")

db.close()

print("===== End of deleteOldPad job =====")
