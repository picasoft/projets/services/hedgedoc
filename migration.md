
```bash
# first temporarily change POSTGRES_USER & POSTGRES_DATABASE to codimd in compose.yml
docker compose up -d db

# Make sure to get last dump
cat /DATA/BACKUP/dbdumps/codimd-db/dump.sql | docker exec -i hedgedoc-db psql -U hedgedoc hedgedoc

docker compose exec db psql -U hedgedoc postgres

	REASSIGN OWNED BY codimd TO hedgedoc;
	\connect codimd
	REASSIGN OWNED BY codimd TO hedgedoc;
	DROP USER codimd
	DROP DATABASE hedgedoc;
	\connect postgres
	ALTER DATABASE codimd RENAME TO hedgedoc;

docker compose up

# crash

